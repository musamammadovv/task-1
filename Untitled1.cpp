#include<bits/stdc++.h>
using namespace std;

int main(){
	int n, k;
	cin >> n;
	int cars[n];
	for(int i = 0; i < n; i++)
		cin >> cars[i];
	cin >> k;
	
	sort(cars, cars + n);
	
	int minimum = cars[n-1];
	for(int i = 0; i < n-k; i++)
		if(minimum > cars[i+k-1]-cars[i])
			minimum = cars[i+k-1]-cars[i];

	cout << minimum;
	return 0;
}
